package org.wujiangbo.service.impl;

import org.wujiangbo.domain.OperLog;
import org.wujiangbo.mapper.OperLogMapper;
import org.wujiangbo.service.IOperLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志记录 服务实现类
 * </p>
 *
 * @author wujiangbo
 * @since 2021-12-21
 */
@Service
public class OperLogServiceImpl extends ServiceImpl<OperLogMapper, OperLog> implements IOperLogService {

}

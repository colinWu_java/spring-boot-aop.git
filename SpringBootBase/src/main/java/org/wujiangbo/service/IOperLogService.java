package org.wujiangbo.service;

import org.wujiangbo.domain.OperLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 操作日志记录 服务类
 * </p>
 *
 * @author wujiangbo
 * @since 2021-12-21
 */
public interface IOperLogService extends IService<OperLog> {

}

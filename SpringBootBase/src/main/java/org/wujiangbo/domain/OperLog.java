package org.wujiangbo.domain;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 操作日志记录
 * </p>
 *
 * @author wujiangbo
 * @since 2021-12-21
 */
@TableName("sys_oper_log")
public class OperLog extends Model<OperLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 日志主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 模块标题
     */
    private String title;
    /**
     * 日志内容
     */
    private String content;
    /**
     * 方法名称
     */
    private String method;
    /**
     * 请求方式
     */
    @TableField("request_method")
    private String requestMethod;
    /**
     * 操作人员
     */
    @TableField("oper_name")
    private String operName;
    /**
     * 请求URL
     */
    @TableField("request_url")
    private String requestUrl;
    /**
     * 请求IP地址
     */
    private String ip;
    /**
     * IP归属地
     */
    @TableField("ip_location")
    private String ipLocation;
    /**
     * 请求参数
     */
    @TableField("request_param")
    private String requestParam;
    /**
     * 方法响应参数
     */
    @TableField("response_result")
    private String responseResult;
    /**
     * 操作状态（0正常 1异常）
     */
    private Integer status;
    /**
     * 错误消息
     */
    @TableField("error_msg")
    private String errorMsg;
    /**
     * 操作时间
     */
    @TableField("oper_time")
    private Date operTime;
    /**
     * 方法执行耗时（单位：毫秒）
     */
    @TableField("take_time")
    private Long takeTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIpLocation() {
        return ipLocation;
    }

    public void setIpLocation(String ipLocation) {
        this.ipLocation = ipLocation;
    }

    public String getRequestParam() {
        return requestParam;
    }

    public void setRequestParam(String requestParam) {
        this.requestParam = requestParam;
    }

    public String getResponseResult() {
        return responseResult;
    }

    public void setResponseResult(String responseResult) {
        this.responseResult = responseResult;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getOperTime() {
        return operTime;
    }

    public void setOperTime(Date operTime) {
        this.operTime = operTime;
    }

    public Long getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(Long takeTime) {
        this.takeTime = takeTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OperLog{" +
        ", id=" + id +
        ", title=" + title +
        ", content=" + content +
        ", method=" + method +
        ", requestMethod=" + requestMethod +
        ", operName=" + operName +
        ", requestUrl=" + requestUrl +
        ", ip=" + ip +
        ", ipLocation=" + ipLocation +
        ", requestParam=" + requestParam +
        ", responseResult=" + responseResult +
        ", status=" + status +
        ", errorMsg=" + errorMsg +
        ", operTime=" + operTime +
        ", takeTime=" + takeTime +
        "}";
    }
}

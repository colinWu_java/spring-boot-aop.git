package org.wujiangbo.query;

import lombok.Data;

/**
 * 基础查询模型
 */
@Data
public class BaseQuery {

    //关键字
    private String keyword;
    //当前页
    private Integer page = 1;
    //每页显示多少条
    private Integer rows = 10;
}
package org.wujiangbo.mapper;

import org.wujiangbo.domain.OperLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 操作日志记录 Mapper 接口
 * </p>
 *
 * @author wujiangbo
 * @since 2021-12-21
 */
public interface OperLogMapper extends BaseMapper<OperLog> {

}

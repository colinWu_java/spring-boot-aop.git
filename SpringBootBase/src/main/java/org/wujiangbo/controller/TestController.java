package org.wujiangbo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.wujiangbo.annotation.MyLog;
import org.wujiangbo.result.JSONResult;

/**
 * 测试接口类
 */
@RestController
@Slf4j
public class TestController {

    //测试
    @GetMapping("/test")
    public JSONResult test(){
        return JSONResult.success();
    }

    @GetMapping("/deleteUserById/{userId}")
    @MyLog(title = "用户模块", content = "删除用户操作")
    public JSONResult deleteUserById(@PathVariable("userId") Long userId){
        //这里具体删除用户代码 省略.....
        return JSONResult.success();
    }

}
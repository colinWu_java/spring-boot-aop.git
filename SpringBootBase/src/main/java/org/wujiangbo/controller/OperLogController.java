package org.wujiangbo.controller;

import org.wujiangbo.service.IOperLogService;
import org.wujiangbo.domain.OperLog;
import org.wujiangbo.query.OperLogQuery;
import org.wujiangbo.result.JSONResult;
import org.wujiangbo.result.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @desc
 * @author wujiangbo
 * @since 2021-12-21
 */
@RestController
@RequestMapping("/operLog")
public class OperLogController {

    @Autowired
    public IOperLogService operLogService;

    /**
     * 保存和修改操作公用此方法
     * @param operLog 前端传递来的实体数据
     */
    @PostMapping(value="/save")
    public JSONResult save(@RequestBody OperLog operLog){
        if(operLog.getId()!=null){
            operLogService.updateById(operLog);
        }else{
            operLogService.insert(operLog);
        }
        return JSONResult.success();
    }

    /**
    * 根据ID删除指定对象信息
    * @param id
    */
    @DeleteMapping(value="/{id}")
    public JSONResult delete(@PathVariable("id") Long id){
            operLogService.deleteById(id);
        return JSONResult.success();
    }

    //根据ID查询对象详情信息
    @GetMapping(value = "/{id}")
    public JSONResult get(@PathVariable("id")Long id)
    {
        return JSONResult.success(operLogService.selectById(id));
    }


    /**
    * 查看所有对象数据（不分页）
    */
    @GetMapping(value = "/list")
    public JSONResult list(){
        return JSONResult.success(operLogService.selectList(null));
    }


    /**
    * 分页查询数据
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping(value = "/pagelist")
    public JSONResult pageList(@RequestBody OperLogQuery query)
    {
        Page<OperLog> page = new Page<OperLog>(query.getPage(),query.getRows());
        page = operLogService.selectPage(page);
        return JSONResult.success(new PageList<OperLog>(page.getTotal(), page.getRecords()));
    }
}